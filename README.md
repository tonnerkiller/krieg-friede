# krieg-friede

[hovercraft](https://github.com/regebro/hovercraft) presentation for ethics class on the concepts of just war and just peace

You can see the presentation on [https://tonnerkiller.gitlab.io/krieg-friede/](https://tonnerkiller.gitlab.io/krieg-friede/)