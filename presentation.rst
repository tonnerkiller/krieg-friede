:title: Gerechter Krieg - Gerechter Friede
:author: Abdul Malak Aljamous, Felix Köhler, Benjamin Koppe
:description: Ein Vortrag über die Konzepte des Gerechten Krieges und des Gerechten Friedens
:keywords: Gerechter Krieg, Gerechter Friede, Ethik, Friedensethik
:css: css/kriegfriede.css
:skip-help: true


.. header::

    .. image:: images/schullogo.jpg
   
----

:class: center

Gerechter Krieg - Gerechter Friede
==================================

Abdul Malak Aljamous, Felix Köhler, Benjamin Koppe
   
----

Das Problem
===========

.. class:: substep

    - Wo es Menschen gibt, gibt es Konflikte

      * beginnt im Sandkasten

      * Extremfall Krieg

    - Krieg hat zwei Seiten

      * Verursacht großes Leid

        + 2\. WK: 50 - 70 Mio. Tote

      * Notwendiges Übel

        + "Kampf ums Überleben / Freiheit" (Aliierte im 2. WK)

        + "Responsibility to Protect" (aktuell Lybien, Syrien, Yemen,...)

----

Gerechter Krieg
===============
.. class:: substep

- Lösung: Konzept des Gerechten Krieges

  * rechter Grund

  * rechte Autorität

  * rechte Absicht

  * begründete Hoffnung auf Erfolg

  * Proportionalität
  
- Vorteile

  * es wird möglich, einen Krieg zu beurteilen

  * Beschränkung des Krieges (in der Theorie)

- Nachteile

  * wer beurteilt den Krieg in der Praxis?

  * Konzept wurde zur Rechtfertigung von Kriegen missbraucht

----

Gerechter Frieden
=================
.. class:: substep

- anderer Blickwinkel: Statt auf Krieg auf Frieden konzentrieren

- Frieden ist mehr als die Abwesenheit von Gewalt

  * Förderung von Freiheit, Würde, Recht

  * Linderung von Not, Unterdrückung, Ausbeutung

- Vorteile

  * Ursachen von Krieg im Fokus

  * Propaganda die Grundlage entzogen

- Nachteile

  * im konkreten Konfliktfall wenig hilfreich

  * Vorwurf: Konzept hat versagt
  
----

Quellen
=======

- `Wikipedia Artikel "Gerechter Krieg" <https://de.wikipedia.org/wiki/Gerechter_Krieg>`_ (Zuletzt abgerufen 16.11.2018)
- Ines Jacqueline Werkner, Antonius Liedhegener (Hrsgg.): Gerechter Krieg – gerechter Frieden. Religionen und friedensethische Legitimationen in aktuellen militärischen Konflikten, Wiesbaden 2009.
- Starck, Christian (Hrsg.): Kann es heute noch gerechte Kriege geben? Göttingen, 2008.
- Strub, Jean Daniel: Der gerechte Friede. Spannungsfelder eines friedensethischen Leitbegriffs, Stuttgart, 2010.

.. class:: small

Präsentation erstellt mit `Hovercraft! <https://github.com/regebro/hovercraft>`_

----

:data-x: r-2000
:data-y: r1500
:data-rotate-x: 180
:data-rotate-y: 180
:data-rotate-z: 90

Fragen ?
========
